# Tecnologias utilizadas
React,
React Router Dom,
Context Api,
Hooks,
Axios,
Styled Components,
Toastify.

## Para Executar o projeto
É necessário executar dois scripts simuntaneamente para simular uma api com os dados mockados e inicializar a aplicação: 'yarn start'(para os dados mockados na porta 8888) e 'yarn start'(para inicializar a aplicação react na porta 3000).
A estrutura de pasta tenho utilizado como base inserir todos na pasta src e organizar cada pasta com a sua devida responsabilidade, a fim de entender melhor o código e o que foi proposto a ser feito.

## O que foi feito?
Listagem dos produtos, filtros através da sidebar e do filter geral, controle de rotas, gerencimento de estado, uso de localStoraga para os dados não serem perdidos ao dar um refresh na aplicação, componentização, configuração da chamada api.

## Pontos a serem melhorados?
Responsividade principalmente para 320px, Alguns detalhes na listagem de produtos.

## Dúvidas
No sidebar tem os filtros por tipo, no entanto os dados mockados só tinha informações dos filtros de cor e gênero, se possível me dar um feedback a respeito disso.

## Obrigado pelo teste, por participar desta oportunidade e até logo!
