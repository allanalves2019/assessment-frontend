import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  :root {
      --gray-light: #d1d1d1;
      --green-light: #61908c;
      --red: #cb0d1f;
      --text-body: #363F5F;
      --shape: #FFFFFF;
      --black: #111111;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body, input, button {
    font-family: 'Open Sans', sans-serif;
  }

  button {
    cursor: pointer;
  }
`;
