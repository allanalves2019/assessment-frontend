import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Category from '../pages/Category';
import Contact from '../pages/Contact';
import Home from './../pages/Home';

function Routes() {
    return (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/category" exact component={Category} />
        <Route path="/contact" exact component={Contact} />
    </Switch>
    )
}

export default Routes;