import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    max-width: 1024px;
    margin: auto;
    flex-direction: column;

    div:first-child {
        padding: 0;
        background: var(--black);
        width: 100vw;
        display: flex;
        justify-content: flex-end;
        
        div {
            display: flex;
            justify-content: flex-end;
            width: 100%;
            max-width: 1024px;
            margin: auto;
            padding: 0.25rem;
            background: transparent;
            font-weight: bold;

            p {
                color: var(--shape);
            }

            a {
                color: var(--shape);

                transition: color 0.2s;

                &:hover {
                    color: var(--red);
                }
            }


        }
    }

    div#search {
        width: 100%;
        padding: 1rem 0;
        display: flex;
        justify-content: space-between;

        section {
            display: flex;
            align-items: center;

            input {
                border: 1px solid var(--gray-light);
                width: 15rem;
                height: 2.25rem;
            }
            
            button {
                border: 1px solid var(--gray-light);
                background: var(--red);
                padding: 0.5rem 0.75rem;
                color: var(--shape);
                font-weight: bold;
                transition: filter 0.2s;

                &:hover {
                    filter: brightness(0.9);
                }
            }
        }
    }

    div:last-child {
        background: var(--red);
        width: 100vw;

        section {
            display: flex;
            justify-content: flex-start;
            width: 100%;
            padding: 0.5rem;
            max-width: 1024px;
            margin: auto;
            gap: 4rem;

            a {
                color: var(--shape);
                font-weight: bold;
                text-decoration: none;
                padding: 1rem;

                &:hover {
                    background: var(--shape);
                    color: var(--red);
                    border-radius: 0.25rem;
                }
            }
        }
        
    }
`; 