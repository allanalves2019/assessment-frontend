import React, { useState } from 'react';

import { Link } from 'react-router-dom';

import { Container } from './styles';

import logo from './../../assets/logo.jpg'
import { useProducts } from '../../contexts/useProducts';
import { useCategory } from '../../contexts/useCategory';
import { toast } from 'react-toastify';

export default function Header() {
    const { filterProducts, getProducts } = useProducts();
    const { allCategory } = useCategory();
    const [filterName, setFilterName] = useState('');

    function filterAllProducts() {
        if (filterName.trim().length === 0) {
            return toast.error('Insira um nome válido na busca do produto!');
        }
        filterProducts(filterName.trim());
        setFilterName('')
    }

    return(
        <Container>
            <div>
                <div>
                    <p>
                        <Link to="/">Acesse sua Conta</Link> ou <Link to="/">Cadastre-se</Link>
                    </p>
                </div>
            </div>
            <div id="search">
                <Link to="/">
                    <img src={logo} alt="Webjump" />
                </Link>
                <section>
                    <input value={filterName} onChange={(e) => setFilterName(e.target.value)} />
                    <button type="button" onClick={filterAllProducts}>BUSCAR</button>
                </section>
            </div>
            <div>
                <section>
                    <Link to="/">PÁGINA INICIAL</Link>
                    {allCategory.length > 0 && allCategory.map(element => (
                        <Link key={element.id} onClick={() => getProducts(element.id)} to="/category">{element.name.toUpperCase()}</Link>
                    )) }
                    <Link to="/contact">CONTATO</Link>
                </section>
            </div>
        </Container>
    )   
}