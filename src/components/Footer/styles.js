import styled from 'styled-components';

export const Container = styled.footer`
    display: flex;
    padding: 4rem;
    background: var(--red);
    width: 100%;
    max-width: 1024px;
    margin: auto;
`;