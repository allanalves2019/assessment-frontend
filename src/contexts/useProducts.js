import React, { createContext, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import api from '../service/Api';

export const ProductsContext = createContext();

export const ProductsProvider = ({ children }) => {
  const storageCategoryId = localStorage.getItem('categoryId');
  const [listProducts, setListProducts] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const history = useHistory();
  const quantityCategories = 3;
  const auxAllProducts = [];
  
  useEffect(() => {
    if (storageCategoryId) {
      getProducts(storageCategoryId);
    }
    async function getAllProducts() {
        try {
            for ( let i = 1; i <= quantityCategories; i++ ) {
                const { data } = await api.get(`categories/${i}`);
                const { items } = data;
                auxAllProducts.push(...items);
                if (i === quantityCategories) {
                  setAllProducts(auxAllProducts);
                  
                }
            }

        } catch (err) {
          console.error(err);

        }
    }
    
    getAllProducts();
  }, []);

  async function getProducts(categoryId) {
    localStorage.setItem('categoryId', categoryId);
    try {
      const { data } = await api.get(`categories/${categoryId}`);
      setListProducts(data);

    } catch(err) {
      console.error(err);

    }
  }

  async function filterProducts(name) {
    const filteredProducts = [];
    allProducts.forEach(element => {
      if (element.name.toUpperCase().startsWith(name.toUpperCase())) {
        filteredProducts.push(element);
        setListProducts({items: filteredProducts});
        history.push('/category')
      } else if (filteredProducts.length === 0) {
        setListProducts([]);
      }
    })
  }

  return (
    <ProductsContext.Provider value={{ allProducts, filterProducts, getProducts, listProducts }}>
      {children}
    </ProductsContext.Provider>
  );
};

export const useProducts = () => useContext(ProductsContext);
