import React, { createContext, useContext, useEffect, useState } from 'react';
import api from '../service/Api';

export const CategoryContext = createContext();

export const CategoryProvider = ({ children }) => {
  const [allCategory, setAllCategory] = useState([]);
  
  useEffect(() => {
    async function getCategory() {
        try {
            const { data } = await api.get('categories/list');
            setAllCategory(data.items);
        } catch(err) {
            console.error(err);
        }
    }
    getCategory();
}, []);

  return (
    <CategoryContext.Provider value={{ allCategory }}>
      {children}
    </CategoryContext.Provider>
  );
};

export const useCategory = () => useContext(CategoryContext);
