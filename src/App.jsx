import { BrowserRouter } from 'react-router-dom';
import Footer from './components/Footer';
import Header from './components/Header';
import { CategoryProvider } from './contexts/useCategory';
import { ProductsProvider } from './contexts/useProducts';
import Routes from './routes/routes';
import { GlobalStyles } from './styles/global';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <BrowserRouter>
      <GlobalStyles/>
      <CategoryProvider>
        <ProductsProvider>
          <Header />
          <Routes />
          <Footer />
        </ProductsProvider>
      </CategoryProvider>
      <ToastContainer />
    </BrowserRouter>
  );
}

export default App;