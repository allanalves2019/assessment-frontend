import { Container, Sidebar, ProductsList, SideProducts } from "./styles";
import { useProducts } from '../../contexts/useProducts'
import { useCategory } from "../../contexts/useCategory";
import { Link } from "react-router-dom";
import FormatCurrency from "../../utils/FormatCurrency";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

export default function Category() {
    const { getProducts, listProducts } = useProducts();
    const [auxProducts, setAuxProducts] = useState([]);
    const { allCategory } = useCategory();
    const colors = [
        {
            name: "Vermelho",
            code: "#cc0d1f" 
        },
        {
            name: "Laranja",
            code: "#ff4500" 
        },
        {
            name: "Azul",
            code: "#28a3aa" 
        }
    ];

    useEffect(() => {
        setAuxProducts(listProducts);
    }, [listProducts])

    function filterColor(name) {
        const filterNameColor = [];
        listProducts.items.forEach(element => {
           if (element.filter[0].color === name) {
                filterNameColor.push(element);
              setAuxProducts({items: filterNameColor});
           }
           if (filterNameColor.length === 0) {
               setAuxProducts(filterNameColor);
           }
        })
    }

    function filterGender(type) {
        const filterTypeGender = [];
        listProducts.items.forEach(element => {
           if (element.filter[0].gender === type) {
              filterTypeGender.push(element);
              setAuxProducts({items: filterTypeGender});
           }
           if (filterTypeGender.length === 0) {
               setAuxProducts(filterTypeGender);
           }
        })
    }

    function buyProduct() {
        toast.success('Produto comprado com sucesso!');
    }

    return(
        <Container>
            <Sidebar>
                <div>
                    <h2>FILTRE POR</h2>
                    <h3>CATEGORIAS</h3>
                    <ul>
                    {allCategory.length > 0 && allCategory.map(element => (
                        <li>
                            <Link key={element.id} onClick={() => getProducts(element.id)} to="/category">{element.name}</Link>
                        </li>
                    ))}
                    </ul>
                    <h3>CORES</h3>
                    <div>
                        {
                            colors.map(({ code, name },index) => {
                                return <button style={{
                                    backgroundColor: code
                                }} key={index} onClick={() => filterColor(name)}></button>
                            })
                        }
                    </div>
                    <h3>GÊNERO</h3>
                    <ul>
                        <li>
                            <a onClick={() => filterGender('Feminina')}>
                                Feminino
                            </a>
                        </li>
                        <li>
                            <a onClick={() => filterGender('Masculina')}>
                                Masculino
                            </a>
                        </li>
                    </ul>
                    <h3>TIPO</h3>
                    <ul>
                        <li>Corrida</li>
                        <li>Caminhada</li>
                        <li>Casual</li>
                        <li>Social</li>
                    </ul>
                </div>
            </Sidebar>
            <SideProducts>
                <ProductsList>
                    {auxProducts && auxProducts.items && auxProducts.items.length > 0 && auxProducts.items.map(element => (
                        <li key={element.id}>
                            <img src={element.image} alt={element.name} />
                            <h4>{element.name}</h4>
                            <p>{element.specialPrice ? FormatCurrency(element.price) : ''}</p>
                            <span>{element.specialPrice ? FormatCurrency(element.specialPrice) : FormatCurrency(element.price)}</span>
                            <button type="button"  onClick={buyProduct}>COMPRAR</button>
                        </li>
                    ))}
                </ProductsList>
                {auxProducts.length === 0 && (
                    <div>
                        <h1>Nenhum produto foi encontrado!</h1>
                    </div>
                )}
            </SideProducts>
        </Container>
    )
}