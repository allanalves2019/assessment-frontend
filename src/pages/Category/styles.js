import styled from 'styled-components';

export const Container = styled.div`
    padding: 1rem;
    display: flex;
    width: 100%;
    max-width: 1024px;
    margin: auto;
    flex-direction: row;
`;

export const Sidebar = styled.div`
    display: flex;
    height: 30rem;
    padding: 0 2rem;
    background-color: var(--shape);
    border: 1px solid var(--gray-light);
    margin-left: 0;
    justify-content: flex-start;
    flex-direction: column;

    div {
        display: flex;
        justify-content: space-between;
        flex-direction: column;
        ul {
            padding: 1rem 2rem;

            li {
                color: gray;

                &:hover {
                    text-decoration : underline;
                    cursor: pointer;
                }
            }

            li > a {
                color: gray;
                text-decoration: none;

                &:hover {
                    text-decoration: underline;
                }
            }

        }

        div {
            padding: 1rem 0 ;
            display: flex;
            flex-direction: row;

            button {
                width: 3rem;
                height: 1.5rem;
                border: none
            }
        }
        
        h2 {
            color: var(--red);
            font-weight: bold;
        }
    
        h3 {
            color: var(--green-light);
        }
    }

`;

export const SideProducts = styled.div`
    display: flex;
    width: 100%;
    margin: 0 0 0 1rem;
    border: 1px solid var(--gray-light);

    div {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;

        h1 {
            color: var(--red);
        }
    }
`;

export const ProductsList = styled.ul`
    display: grid;
    margin: 0 auto;
    padding: 1rem;
    grid-template-columns: repeat(4, 1fr);
    color: black;
    grid-gap: 2rem;
    list-style: none;
    text-align: center;

    @media only screen and (max-width: 768px) {
        grid-template-columns: repeat(3, 1fr);
    }

    @media only screen and (max-width: 320px) {
        grid-template-columns: repeat(2, 1fr);
    }

    li {
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        max-height: 18rem;

        img {
            border: 1px solid var(--gray-light);
            width: 10rem;
            height: 10rem;
        }

        p {
            color: gray;
            height: 1rem;
            text-decoration: line-through;
        }

        h4 {
            color: gray;
        }

        span {
            font-weight: bold;
            font-size: 1.3rem;
            color: var(--red);
        }

        button {
            padding: 0.5rem;
            background: var(--green-light);
            color: var(--shape);
            font-weight: bold;
            border: none;
            border-radius: 0.25rem;
            transition: filter 0.2s;

            &:hover {
                filter: brightness(0.9);
            }
        }
    }
`;