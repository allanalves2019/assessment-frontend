import { Link } from 'react-router-dom';
import { useCategory } from '../../contexts/useCategory';
import { useProducts } from '../../contexts/useProducts';
import { Container, Sidebar, SideContact } from './styles';

export default function Contact() {
    const { allCategory } = useCategory();
    const { getProducts } = useProducts();

    return(
        <Container>
            <Sidebar>
                <uL>
                    <li><Link to="/">Página Inicial</Link></li>
                    {allCategory.length > 0 && allCategory.map(element => (
                        <li>
                            <Link key={element.id} onClick={() => getProducts(element.id)} to="/category">{element.name}</Link>
                        </li>
                    ))}
                    <li><Link to="/contact">Contatos</Link></li>
                </uL>
            </Sidebar>
            <SideContact>
                <h1>Contatos</h1>
                <p>
                    <b>Endereço:</b> Av. Pres. Juscelino Kubitschek, 50 – 7º andar  – Vila Nova Conceição
                    São Paulo – SP • CEP: 04543-000 • Brasil
                </p>
                <p>
                    <b>Telefone:</b> +55 11 2338-6889
                </p>
                <p>
                    <b>E-mail:</b> contato@webjump.com.br
                </p>
            </SideContact>
        </Container>
    )
}