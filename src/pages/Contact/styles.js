import styled from 'styled-components';

export const Container = styled.div`
    padding: 1rem;
    display: flex;
    width: 100%;
    max-width: 1024px;
    margin: auto;
    flex-direction: row;
`;

export const Sidebar = styled.div`
    display: flex;
    height: 30rem;
    width: 15rem;
    padding: 0 2rem;
    background-color: var(--gray-light);
    border: 1px solid var(--gray-light);
    margin-left: 0;
    justify-content: flex-start;
    flex-direction: column;

    ul {
        padding: 1rem;
        display: flex;
        flex-direction: column;
        gap: 1rem;
        color: gray;
        text-decoration: none;

        li > a {
            color: gray;
            text-decoration: none;

            &:hover {
                text-decoration: underline;
            }
        }
    }
`;

export const SideContact = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 0 0 0 1rem;
    gap: 2rem;

    div {
        height: 50%;
        width: 100%;
        background: gray;
    }
    h1 {
        color: gray;
    }

    p {
        text-align: justify;
        color: gray;
    }
`;